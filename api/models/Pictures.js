/**
* Pictures.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes: {

     id: {
      type: 'FLOAT',
      primaryKey: true,
      autoIncrement: true
    },
  	url:{
  		type:'string',
  		required: true
  	},
  	univ:{
  		type:'string',
  		required: true
  	}

  }
};

