/**
* Univ.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    // Description générale

  	name:{
  		type:'string',
  		required: true
  	},

  	shortDes:{   // Short description
        type:'string'
  	},

    profilPic:{  // Link to the profil pic
      type: 'string'
    },

    adresse:{
      type: 'string'
    },

    pays:{
      type: 'string',
      required: true
    },

    ville:{
      type: 'string',
      required: true
    },

    lat:{
      type: 'string'
    },

    longi:{
      type: 'string'
    },
    
    //Procédure à dauphine

    moyenne:{
    	type: 'string' // Moyenne des étudiants qui partent
    },
    
    scores:{
    	type: 'string' // Score TOEFL/entretien de langue
    },

    demande:{   //Tres demandee, moyen, peu
      type: 'string'
    },

    filliere:{
      type: 'string'
    },

    langue:{ // fr, en, es, po, al, it
      type: 'string'
    },

    //Pour le grand départ

    prixBillet:{
      type: 'string'
    },

    visa:{
      type: 'string'
    },

    fees:{  // Cout inscription
      type:'string'
    },

    majorite:{
      type:'string'
    },

    //Vie pratique
  
    transport:{      //Pour se rendre dans le pays
      type:'string'
    },
  
    budget:{   //Budget à prévoir logement nourriture sortie
      type:'string'
    },

    logement:{
      type:'string'
    },

    moove:{   //Deplacement sur place
      type:'string'
    },

    //Vie sur le campus

    nourriture:{
      type:'string'
    },

    coutumes:{
      type:'string'
    },

    cadre:{    // Structure edt 
      type:'string'
    },

    cours:{ // Cours choisis + niveau (+ = -)
      type:'string'
    },

    courses:{
      type:'string'  // Liste des cours à choisir
    },

    // Voyager et bien plus

    voyages:{
      type:'string'
    },

    bonPlans:{
      type:'string'
    },

    // Autres

    vieEtudiante:{
      type:'string'
    },

    travailDemande:{
      type:'string'
    },

    diffiLangue:{
      type:'string'
    },

    link:{
      type:'string'
    }

  }
};



