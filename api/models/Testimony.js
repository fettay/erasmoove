/**
* Testimony.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/
var moment = require('moment');

module.exports = {

  attributes: {

  	auth:{
  		type:'string',
  		required:true
  	},

    auth_fil:{
      type:'string',
      required:true
    },

    year:{
      type:'string'
    },

  	content:{
  		type:'string',
  		required:true,
      size: 20000
  	},

  	univ:{
  		type:'string',
  		required:true
  	},


    createAtFormated: function(){
      var dateFormatee = new Date();
      dateFormatee = this.createdAt;
      return moment(dateFormatee).lang('fr').format('LL')+' à '+moment(dateFormatee).hour()+':'+moment(dateFormatee).minutes();
    }
  
  }
};

