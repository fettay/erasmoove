/**
 * UnivController
 *
 * @description :: Server-side logic for managing univs
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

	'new': function(req,res){
        if(req.session.authenticated){
            res.locals.flash = _.clone(req.session.flash);
            res.view();
            req.session.flash = {};
        }
        else{
            res.redirect('session/new');
        }
    },

    create: function(req, res, next){
     if(req.session.authenticated){ 
   		Univ.create(req.params.all(), function adminCreated(err, univ){
      		if(err){
        		req.session.flash = {
              		err: 1,   // General error
              		message: "Vérifier que le nom est bien inséré."
           	 	}
            	console.log(err);
            	return res.redirect('/univ/new');         
           	}
           	req.session.flash = {};
           	res.redirect('/admin/profil/'+req.session.user.id);
        });
     }
     else{
      res.redirect('session/new');
     }
  	},

    profil: function(req,res,next){
      Univ.findOne(req.param('id'), function univFound(err, univ){
        if(err) return next(err);
        if(!univ) return next();
        Testimony.find().where({univ:req.param('id')}).exec(function(err,testis){
          Pictures.find().where({univ:req.param('id')}).exec(function(err,pics){
            res.view({ univ: univ, testis:testis, pics:pics,authenticated: req.session.authenticated});
          });
        });
      });
    },

   edit: function(req, res, next){
    if(req.session.authenticated){
      Univ.findOne(req.param('id'), function editUniv(err, univ){
        if(err) return next(err);
        if(!univ) return next();
        res.locals.flash = _.clone(req.session.flash); 
        res.view({
          univ: univ
        });
        req.session.flash = {};
      });
    }
    else{
      res.redirect('session/new')
    }
  },

  update: function(req, res, next){
    if(req.session.authenticated){
      Univ.update(req.param('id'),req.params.all(), function updated(err){
        if(err){
          console.log(err);
          req.session.flash = {
            err:1,
            message:"Erreur lors de l'édition"
          }
        res.redirect('univ/edit/'+req.param('id'));
        }
        else{
          res.redirect('admin/profil/'+req.session.user.id);
        }      
      });
    }
    else{
      res.redirect('session/new');
    }
  },

  destroy: function(req,res,next){
    if(req.session.authenticated){
      Univ.destroy(req.param('id'), function(err){
        if(err){
          return next(err);
        }
        else{
          res.redirect('admin/profil/'+req.session.user.id);
        }
      });
    }
    else{
      res.redirect('session/new')
    }
  }

};

