/**
 * TestimonyController
 *
 * @description :: Server-side logic for managing Testimonies
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

	create: function(req,res,next){
    console.log(req.params.all());
 	 Testimony.create(req.params.all(), function testiCreated(err, testi){
      if(err){
        req.session.flash = {
              err: 1,   // General error
              message: "Veuillez vérifier que tous les champs ont bien été remplis",
            }
            res.redirect('/');         
          }
          else{
            res.redirect('/univ/profil/'+testi.univ);
          }
	  });
 	},
	
};

