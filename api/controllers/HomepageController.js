module.exports = {

    homepage: function(req, res) {
        Univ.find().exec(function(err, uni) {
            res.view('homepage',{uni:uni});
        });
    },

    detail: function(req, res) {
        Univ.findOne(req.param('id'), function(err, uni) {
            res.view({uni : uni});
        });
    },

    versJSON: function(req,res){
        if(req.param('lang')=="0" && req.param('fil')=="0"){
            Univ.find().exec(function(err,uni){
                return res.json(uni);
            }); 
        }        

        else if(req.param('fil')=="0"){
            Univ.find({langue: req.param('lang') }).exec(function(err,uni){
                return res.json(uni);
            }); 
        }

        else if(req.param('lang')=="0"){
            Univ.find({filliere: req.param('fil')}).exec(function(err,uni){
                return res.json(uni);
            }); 
        }

        else{
            Univ.find({filliere: req.param('fil'), langue: req.param('lang') }).exec(function(err,uni){
                return res.json(uni);
            }); 
        }

    },

    query: function(req,res,next){
        var clean= function(s){     // Remove accent, and put it to lowercase
            var r=s.toLowerCase();
            r = r.replace(new RegExp(/\s/g),"");
            r = r.replace(new RegExp(/[àáâãäå]/g),"a");
            r = r.replace(new RegExp(/æ/g),"ae");
            r = r.replace(new RegExp(/ç/g),"c");
            r = r.replace(new RegExp(/[èéêë]/g),"e");
            r = r.replace(new RegExp(/[ìíîï]/g),"i");
            r = r.replace(new RegExp(/ñ/g),"n");                
            r = r.replace(new RegExp(/[òóôõö]/g),"o");
            r = r.replace(new RegExp(/œ/g),"oe");
            r = r.replace(new RegExp(/[ùúûü]/g),"u");
            r = r.replace(new RegExp(/[ýÿ]/g),"y");
            r = r.replace(new RegExp(/\W/g),"");
            return r;
        }
        Univ.find({
            or : [
            { 
                name: {
                    'contains':clean(req.param('word'))
                }   
            },
            { 
                pays: {
                    'contains':req.param('word')
                }   
            },
            { 
                ville: {
                    'contains':req.param('word')
                }   
            },                          
            ]
        }).exec(function(err,uni){
            if(err) return next(err);
            res.view('search',{ uni : uni, ask: req.param('word')});
        });
    },
        query_json: function(req,res,next){      // For Autocomplete
            var clean= function(s){     // Remove accent, and put it to lowercase
            var r=s.toLowerCase();
            r = r.replace(new RegExp(/\s/g),"");
            r = r.replace(new RegExp(/[àáâãäå]/g),"a");
            r = r.replace(new RegExp(/æ/g),"ae");
            r = r.replace(new RegExp(/ç/g),"c");
            r = r.replace(new RegExp(/[èéêë]/g),"e");
            r = r.replace(new RegExp(/[ìíîï]/g),"i");
            r = r.replace(new RegExp(/ñ/g),"n");                
            r = r.replace(new RegExp(/[òóôõö]/g),"o");
            r = r.replace(new RegExp(/œ/g),"oe");
            r = r.replace(new RegExp(/[ùúûü]/g),"u");
            r = r.replace(new RegExp(/[ýÿ]/g),"y");
            r = r.replace(new RegExp(/\W/g),"");
            return r;
        };
            Univ.find({
                or : [
                { 
                    name: {
                        'contains':req.param('word')
                    }   
                },
                { 
                    pays: {
                        'contains':req.param('word')
                    }   
                },
                { 
                    ville: {
                        'contains':req.param('word')
                    }   
                },                          
                ]
            }).exec(function(err,univ){
                var result=[];
                _.each(univ, function(uni){
                    if(err) return next(err);
                    console.log(clean(uni.name).indexOf(clean(req.param('word')))>-1);
            if(clean(uni.name).indexOf(clean(req.param('word')))>-1){  // Trouve l'élément qui match
                var indic={
                    indi: uni.name
                }
                console.log('here');
            }
            if(clean(uni.pays).indexOf(clean(req.param('word')))>-1){  // Trouve l'élément qui match
               var indic={
                    indi: uni.pays
                }
            }
            if(clean(uni.ville).indexOf(clean(req.param('word')))>-1){  // Trouve l'élément qui match
               var indic={
                    indi: uni.ville
                }
            }
            result.push(indic);
            
        });
                res.json(result);
            });     
        },
        query_open: function(req,res,next){

            Univ.find({
                or : [
                { 
                    name: {
                        'contains':req.param('word')
                    }   
                },
                { 
                    pays: {
                        'contains':req.param('word')
                    }   
                },
                { 
                    ville: {
                        'contains':req.param('word')
                    }   
                },                          
                ]
            }).exec(function(err,univ){
                // res.header('Access-Control-Allow-Origin', '*');
                res.json(univ);
            });     

        }

    };
