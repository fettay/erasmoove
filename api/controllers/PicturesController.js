/**
 * PicturesController
 *
 * @description :: Server-side logic for managing pictures
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
 upload: function(req,res,next){
    req.file('picture').upload({maxBytes:10000000,dirname:'../../assets/img/univs'},function(err,file){
      if(err) return next(err);
      if (file.length === 0){
        return res.badRequest('No file was uploaded');
      }
      var link = file[0].fd.substring(file[0].fd.indexOf('/img'),file[0].fd.length);
      Univ.find().where({id:req.param('univ')}).exec(function(err,univ){
        if(univ){
          console.log(univ);
          if(!univ.profilPic){
            Univ.update({id:req.param('univ')}, {profilPic:link},function(){console.log('done')});
          }
        }
      });
      Pictures.create({url:link,univ:req.param('univ')},function(err,pic){
      	if(err) return next(err);
      	res.redirect('univ/profil/'+pic.univ)
      });
    });

  },

  remove: function(req,res,next){
      Pictures.destroy(req.param('name'),function(err){
        if(err) return next(err); 
        res.status(200).end();
        console.log(res);
        });
  }
};

