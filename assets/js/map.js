var markers = [];
var map;

function initialize() {

  var universites;
    $.ajax(
{
  // dataType: "jsonp",
  url: "/json/0&0",
  async: false,
  success:function(data){
    universites = data;
    //$.each(data, function(i,item){
    }  
  ,
  error:function(err){
  }


});

        
        var mapOptions = {
          center: { lat: 20, lng: 2.3488000},
          zoom: 2,
          scrollwheel: false,
          disableDefaultUI: true,
                    styles: [
            {stylers: [{ visibility: 'simplified' }]},
            {elementType: 'labels', stylers: [{ visibility: 'off' }]}
            ]
        };
        
        map = new google.maps.Map(document.getElementById('map'), mapOptions);

        var styles = [
  {
    stylers: [
      { hue: "#75BFD1" },
      { saturation: -20 }
    ]
  },{
    featureType: "road",
    elementType: "geometry",
    stylers: [
      { lightness: 100 },
      { visibility: "simplified" }
    ]
  },{
    featureType: "road",
    elementType: "labels",
    stylers: [
      { visibility: "off" }
    ]
  }
];

map.setOptions({styles: styles});



  $.each(universites, function(i,item){

    addUniversite(universites[i]);
});
}
      google.maps.event.addDomListener(window, 'load', initialize);


function setAllMap(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}


function addUniversite(universite) {

  var myLatlng = new google.maps.LatLng(universite.lat,universite.longi);


          var marker = new Marker({
          map: map,
          title: 'universite',
          position:  myLatlng,
          icon: {
            path: MAP_PIN,
            fillColor: '#0E77E9',
            fillOpacity: 0.8,
            strokeColor: '',
            strokeWeight: 0,
            scale: 1/4
          },
          label: '<i class="map-icon-university"></i>'
        });

  marker.info = new google.maps.InfoWindow({
  content: '<div style="width:300px; height:100px"><h1>'+universite.name+'</h1>'
    +'<p>'+universite.shortDes+'</p>'+
    '<p><a href="/univ/profil/'+universite.id+'">Plus de détails</a></p></div>'
});

  google.maps.event.addListener(marker, 'click', function() {
    marker.info.open(map,marker);
  });
  markers.push(marker);
}


function setContinent(continent) {

  var country = continent

  var geocoder = new google.maps.Geocoder();
  geocoder.geocode( { 'address': country }, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      map.setCenter(results[0].geometry.location);
      if(country == "World")
        map.setZoom(2);
      else
        map.setZoom(3);
    } else {
      alert("Could not find location: " + location);
    }
  });

}